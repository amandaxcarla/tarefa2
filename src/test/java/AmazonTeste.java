import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class AmazonTeste {

    private WebDriver driver;

    @Before
    public void abrir() {
        System.setProperty("webdriver.gecko.driver","C:/Driver/geckodriver.exe");
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.get("https://www.amazon.com.br/");
    }

    @After
    public void sair() {
        driver.quit();
    }

    @Test
    public void maisVendidos() throws InterruptedException {
        Thread.sleep(3000);
        driver.findElement(By.id("nav-hamburger-menu")).click();
        Thread.sleep(3000);
        driver.findElement(By.xpath("/html/body/div[3]/div[2]/div/ul[1]/li[2]/a")).click();
        Thread.sleep(3000);
        Assert.assertEquals("Mais vendidos", driver.findElement(By.id("zg_banner_text")).getText());
        Thread.sleep(3000);


    }

    @Test
    public void testarBuscador() throws InterruptedException {
        driver.findElement(By.id("twotabsearchtextbox")).sendKeys("Qualquer");
        driver.findElement(By.id("nav-search-submit-button")).click();
        Thread.sleep(3000);
        Assert.assertEquals("RESULTADOS", driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[1]/div[1]/div/span[3]/div[2]/div[1]/div/span/div/div/span")).getText());
        Thread.sleep(3000);
    }

    @Test
    public void testarAutenticacaoSenhaIncorreta() throws InterruptedException {
        driver.findElement(By.id("nav-link-accountList")).click();
        driver.findElement(By.id("ap_email")).sendKeys("shaushaus@gmail.com");
        driver.findElement(By.id("continue")).click();
        Assert.assertEquals("Houve um problema", driver.findElement(By.className("a-alert-heading")).getText());
        Thread.sleep(3000);

    }


    @Test
    public void testarAutenticacaoSenhaIncorreta() throws InterruptedException {
        driver.findElement(By.id("nav-link-accountList")).click();
        driver.findElement(By.id("ap_email")).sendKeys("shaushaus@gmail.com");
        driver.findElement(By.id("continue")).click();
        Assert.assertEquals("Houve um problema", driver.findElement(By.className("a-alert-heading")).getText());
        Thread.sleep(3000);
    }

    @Test
    public void abaLivros() throws InterruptedException {
        Thread.sleep(5000);
        driver.findElement(By.xpath("/html/body/div[1]/header/div/div[4]/div[2]/div[2]/div/a[5]")).click();
        Thread.sleep(5000);
        driver.findElement(By.id("twotabsearchtextbox")).sendKeys("Livros");
        driver.findElement(By.id("nav-search-submit-button")).click();
        Thread.sleep(5000);
        Assert.assertEquals("RESULTADOS", driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[1]/div[1]/div/span[3]/div[2]/div[2]/div/span/div/div/span")).getText());
        Thread.sleep(5000);
    }

@Test
    public void navegarMaisVendidos() throws InterruptedException {
        Thread.sleep(5000);
        driver.findElement(By.xpath("/html/body/div[1]/header/div/div[4]/div[1]")).click();
        Thread.sleep(3000);
        driver.findElement(By.xpath("/html/body/div[3]/div[2]/div/ul[1]/li[3]/a")).click();
        Thread.sleep(5000);
        Assert.assertEquals("Novidades na Amazon", driver.findElement(By.id("zg_banner_text")).getText());
        Thread.sleep(3000);

}

    @Test
    public void navegarNovidasnaAmazon() throws InterruptedException {
        Thread.sleep(5000);
        driver.findElement(By.xpath("/html/body/div[1]/header/div/div[4]/div[1]")).click();
        Thread.sleep(3000);
        driver.findElement(By.xpath("/html/body/div[3]/div[2]/div/ul[1]/li[3]/a")).click();
        Thread.sleep(5000);
        Assert.assertEquals("Novidades na Amazon", driver.findElement(By.id("zg_banner_text")).getText());
        Thread.sleep(3000);

    }
@Test
    public void navegarProdutosemAlta() throws InterruptedException {
        Thread.sleep(5000);
        driver.findElement(By.xpath("/html/body/div[1]/header/div/div[4]/div[1]/a/span")).click();
        Thread.sleep(5000);
        driver.findElement(By.xpath("/html/body/div[3]/div[2]/div/ul[1]/li[4]/a")).click();
        Thread.sleep(5000);
        Assert.assertEquals("Produtos em alta", driver.findElement(By.id("zg_banner_text")).getText());
    }
@Test
    public void Ofertasdodia() throws InterruptedException {
        Thread.sleep(5000);
        driver.findElement(By.xpath("/html/body/div[1]/header/div/div[4]/div[2]/div[2]/div/a[3]")).click();
        Thread.sleep(5000);
        Assert.assertEquals("Ofertas e Promoções", driver.findElement(By.xpath("/html/body/div[1]/div[4]/div[2]/div/div/h1")).getText());
    }
}